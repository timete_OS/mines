import unittest

import no_app
import schemas


bob = schemas.UserCreate(
    name="bob",
    pswd="canard",
)

timo = schemas.UserCreate(
    name="Timo",
    pswd="canard",
)


class TestUserCreation(unittest.IsolatedAsyncioTestCase):

    async def base_test(self):
        response = await no_app.create_user(bob)
        self.assertIsInstance(response, schemas.UserRead)

    async def test_user_already_exist(self):
        response = await no_app.create_user(timo)
        print(type(response))
        self.assertIsInstance(response, no_app.HTTPException)


if __name__ == '__main__':
    unittest.main()
