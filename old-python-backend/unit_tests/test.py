

class A:

    def __init__(self, first, **kwargs) -> None:
        self.first = first

class B(A):

    def __init__(self, first, second) -> None:
        super().__init__(first)
        self.second = second


a = A(1)

b = B(1, 2)

print(vars(b))
g = super(B, b).__new__(A)
#h = super(A, b).__init__(**vars(b))

g.__init__(**vars(b))

print(f'{type(g)}, {(vars(g))}')