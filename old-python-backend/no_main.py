import uvicorn
import os

from dotenv import load_dotenv
from no_app import app

load_dotenv()

port = os.environ.get("PORT")
if port is None:
    port = os.environ.get("API_PORT")

port = int(port)
host = os.environ.get("HOST")

if __name__ == "__main__":
    uvicorn.run(app, host=host, port=port)
