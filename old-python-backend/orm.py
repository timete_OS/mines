import hashlib

from datetime import date, datetime, timedelta

from sqlalchemy import and_, or_, desc
from schemas import UserBase, User, GameBase, Game, Token


class ORM:

    def __init__(self) -> None:
        pass


    def get_all(self, session, ObjectType, skip: int = 0, limit: int = 100, order = None):
        if order is not None:
            # wrong behavior to report here
            # != None should be is not None
            return session.query(ObjectType)\
                    .filter(order != None)\
                        .order_by(order)\
                            .offset(skip)\
                                .limit(limit).all()
        return session.query(ObjectType)\
                .offset(skip)\
                    .limit(limit).all()

    #  sqlalchemy.orm.Query.exists() here may be useful ? nope
    def get_with(self, session, ObjectType, limit: int = 100, order = None, **user_attribute):
        if order is None:
            return session.query(ObjectType).filter(
                and_(
                    getattr(ObjectType, key) == item for key, item in user_attribute.items()
                )
            ).limit(limit).all()
        return session.query(ObjectType).filter(
            and_(
                getattr(ObjectType, key) == item for key, item in user_attribute.items()
            )
        ).order_by(desc(order)).limit(limit).all()

    """ def get_with_or(self, session, ObjectType, limit: int = 100, **user_attribute):
        return session.query(ObjectType).filter(
        or_(
            getattr(ObjectType, key) == item for key, item in user_attribute.items()
            )
        ).limit(limit).all() """


    # def get_games_from_user(self, session, user_id: str):  # -> list[Game]:

    # def update_user(self, session, user: User, **kwargs):
    #     session.query(User).\
    #         filter(User.id == user.id).\
    #         update(kwargs)
    #     session.commit()
    #     return user

        

    def populate_db(self, session):
        timo = User(**{'name': "Timo",
                       'hashed_pswd': hashlib.sha256("canard".encode()).hexdigest(),
                       'member_since': date(2021, 9, 1),
                       'id' : 1,
                       'best_time' : timedelta(minutes=24, seconds=32)
                        })

        bob = User(**{'name': "Bob",
                           'hashed_pswd': hashlib.sha256("bob".encode()).hexdigest(),
                           'member_since': date(2021, 10, 12),
                           'id' : 2
                           })

        rems = User(**{'name': "Rémi",
                         'hashed_pswd': hashlib.sha256("Pépinou".encode()).hexdigest(),
                         'member_since': date(2022, 3, 21),
                         'id' : 3,
                         'best_time' : timedelta(minutes=28, seconds=32)
                         })
        
        session.expire_on_commit = False

        for user in [timo, bob, rems]:
            # db_user = User.from_orm(user)
            session.merge(user)
        session.commit()


        partie0 = GameBase(
            player_id=1,
            mines_number=43,
            date=datetime.fromisoformat("2021-05-15T15:54:48"),
            time=timedelta(minutes=12, seconds=32)
        )

        partie1 = GameBase(
            player_id=1,
            mines_number=333,
            date=datetime.fromisoformat("2021-06-15T15:54:48"),
            time=timedelta(minutes=4, seconds=32)
        )

        partie2 = GameBase(
            player_id=1,
            mines_number=431,
            date=datetime.fromisoformat("2021-06-15T16:00"),
            time=timedelta(minutes=20, seconds=9)
        )

        partie3 = GameBase(
            player_id=3,
            mines_number=23,
            date=datetime.fromisoformat("2021-07-15T16:00"),
            time=timedelta(minutes=8, seconds=32)
        )



        for game in (partie0, partie1, partie2, partie3):
            db_game = Game.from_orm(game)
            session.merge(db_game)
        session.commit()



        token = Token(token_value="coin coin", user_id=1)
        session.merge(token)
        session.commit()
