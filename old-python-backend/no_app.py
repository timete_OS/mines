import base64
import time
import logging
import hashlib
import secrets
import os

# import dateutil.parser
from typing import List, Union
from dotenv import load_dotenv
from datetime import date, datetime

from fastapi import Depends, FastAPI, HTTPException, Header, status
from fastapi.middleware.cors import CORSMiddleware
from contextlib import asynccontextmanager


from sqlalchemy import create_engine
from sqlalchemy.orm import Session


from sqlmodel import Session, SQLModel, create_engine

logging.basicConfig(filename='infos.log', encoding='utf-8', level=logging.INFO)


load_dotenv()

# host = os.environ.get("DATABASE_HOST")
# username = os.environ.get("DATABASE_USERNAME")
# password = os.environ.get("DATABASE_PASSWORD")
# name = os.environ.get("DATABASE_NAME")
# dbport = os.environ.get("MYSQL_PORT")


developpment_url = os.environ.get("DATABASE_URL")
postgres_connection_string = os.environ.get("DATABASE_PRIVATE_URL")

if postgres_connection_string is None:
    connection_string = developpment_url
else:
    # connection_string = postgres_connection_string.replace('postgresql://', 'postgresql+psycopg2://')
    connection_string = postgres_connection_string


# could not translate host name "postgres.railway.internal" to address: Name or service not known
# this error persists only when I re-deploy and I have NO IDEA WHY

connect_args = {"check_same_thread": False, "echo": False}

print('used connection string', connection_string)
engine = create_engine(connection_string)


def create_db_and_tables():
    print('creating tables')
    SQLModel.metadata.create_all(engine)

from schemas import *
from orm import ORM

@asynccontextmanager
async def lifespan(app: FastAPI):
    print('sleeping 1s')
    time.sleep(1) # if this solves the railway re-deployment problem I am killing myself
    create_db_and_tables()
    db.populate_db(session=Session(engine))
    create_user_table()
    yield


db = ORM()
app = FastAPI(lifespan=lifespan)


origins = [
    "http://thispainofmine.com",
    "https://thispainofmine.com",
    "*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

def get_session():
    session = Session(engine)
    try:
        yield session
    finally:
        session.close()


# check if token is valid
def token_to_user(token):
    if token not in user_table:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
            headers={"WWW-Authenticate": "Bearer"}
        )
    return user_table[token]

# SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def create_user_table(session=Session(engine)):
    """Create a table of known tokens and their associated users
    Allows for quick verification of requests validity.

    Args:
        session (_type_, optional): Defaults to SessionLocal().
    """
    global user_table
    # ici il me faut une fonction qui cast un User vers un UserBase
    user_table = {
        token.token_value: db.get_with(session, User, id=token.user_id)[0]
        for token in db.get_all(session, Token)}
    # session.close()

# @app.on_event("startup")
# def on_startup():
#     print('sleeping 1s')
#     time.sleep(1) # if this solves the railway re-deployment problem I am killing myself
#     create_db_and_tables()
#     db.populate_db(session=Session(engine))
#     create_user_table()




@app.get("/")
def root():
    return {"True": False}


@app.put("/user", response_model=Token)
async def create_user(user: UserCreate, session: Session = Depends(get_session)):
    print("CREATING A USER")
    L = db.get_with(session, User, name=user.name)
    if L:
        raise HTTPException(status_code=400, detail="User already exist")
    t = date.today()
    hashed_pswd = hashlib.sha256(user.pswd.encode()).hexdigest()
    token_value = secrets.token_hex(32) + hashed_pswd
 
    user = UserBase(name=user.name, hashed_pswd=hashed_pswd, member_since=t)
    db_user = User.from_orm(user)
    print(db_user)
    session.add(db_user)
    session.commit()
    session.refresh(db_user)

    user_table[token_value] = db_user

    token = Token(token_value=token_value, user_id=db_user.id)
    session.add(token)
    session.commit()
    session.refresh(token)

    return token


@app.get("/login", response_model=Token)
async def login(name: str, pswd: str, session: Session = Depends(get_session)):
    pswd = pswd.encode()
    m = hashlib.sha256(pswd).hexdigest()
    L = db.get_with(session, User, name=name, hashed_pswd=m)
    if L:
        user = L[0]
        # pas sur de capter ici
        # mais il faut signer le token avec le mdp ?
        # je pense que l'idée c'est surtout d'update le token avec le hash du mdp
        # à chaque requête, pour qu'un gars au milieu ne puisse rien faire
        value = secrets.token_hex(32) + hashlib.sha256(pswd).hexdigest()

        user_table[value] = user

        token = Token(token_value=value, user_id=user.id)
        print(f"Le token : {token}")
        session.add(token)
        session.commit()
        session.refresh(token)
        return token
    raise HTTPException(status_code=400, detail="Wrong auth")

@app.get("/user/{user_id}", response_model=UserRead)
async def read_user(user_id: int, session: Session = Depends(get_session)):
    user = session.get(User, user_id)
    if user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return user


@app.get("/users", response_model=List[UserRead])
async def read_users(skip: int = 0, limit: int = 100, session: Session = Depends(get_session)):
    users = db.get_all(session, User, skip=skip, limit=limit)
    return users

@app.get("/sorted/users", response_model=List[UserRead])
async def read_sorted_users(skip: int = 0, limit: int = 100, session: Session = Depends(get_session)):
    users = db.get_all(session, User, skip=skip, limit=limit, order=User.best_time)
    return users

@app.get("/who_am_I", response_model=UserRead)
async def who_am_I(token: Union[str, None] = Header(default=None)):
    return token_to_user(token)


@app.put("/game", response_model=Game)
async def create_game(game: GameCreate, passcode: str, token: Union[str, None] = Header(default=None), session: Session = Depends(get_session)):
    t = datetime.now()
    if passcode:
        # check if request comes from the actual interface
        key = list(bytes(os.environ.get("INTERFACE_KEY"), encoding='utf8'))
        print("passcode", passcode)
        try:
            bytes_passcode = base64.b64decode(passcode)
            print("base64 decoding is fine")
            l = list(range(256))
            substition = l[-3:] + l[:-3]
            res = [substition[x] ^ y for x, y in zip(list(bytes_passcode), key)]
            res = bytes(res[:16]).decode()
            if res != token[:16]:
                raise Exception
        except:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Who does the malin, falls in the ravin"
                )
    print(f"le token :  {token}")
    user = user_table.get(token)
    if user is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
            headers={"WWW-Authenticate": "Bearer"}
        )
    L = db.get_with(session, Game, order=Game.date, player_id=game.player_id)
    if L:
        delay = t - L[0].date
        print(f"there has been a delay of {delay} between two games\n")
        if delay.seconds < 3:
            raise HTTPException
    db_game = Game.from_orm(game, update={"date": t})
    db_user = db.get_with(session, User, id=game.player_id)
    if db_user:
        db_user = db_user[0]
    if (db_user.best_time is None) or (db_user.best_time > game.time):
        db_user.best_time = game.time
    session.add(db_game)
    session.commit()
    session.refresh(db_game)
    return db_game


@app.get("/games", response_model=List[Game])
async def read_games(skip: int = 0, limit: int = 100, session: Session = Depends(get_session)):
    games = db.get_all(session, Game, skip=skip, limit=limit)
    return games



@app.post("/user", response_model=UserRead)
async def update_user(best_time:timedelta, token: Union[str, None] = Header(default=None), session: Session = Depends(get_session)):
    user = token_to_user(token)
    print(f"\n le user : {user}")
    session.query(User).\
        filter(User.id == user.id).\
        update({'best_time': best_time,
                # 'name' : name,
                })
    session.commit()
    return user
