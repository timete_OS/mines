from datetime import datetime, date, timedelta
from uuid import uuid4


from typing import Optional

from sqlmodel import Field, SQLModel


print("importing schemas")

class UserCreate(SQLModel):
    name: str
    pswd: str


class UserBase(SQLModel):
    name: str# = Field(index=True)
    hashed_pswd: str
    member_since: date
    best_time: Optional[timedelta] = Field(default=None)


class User(UserBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)


class UserRead(SQLModel):
    name: str
    member_since: date
    id: int
    best_time: Optional[timedelta] = Field(default=None)


class GameCreate(SQLModel):
    mines_number: int
    player_id: int = Field(foreign_key="user.id")
    time: timedelta

class GameBase(GameCreate):
    date: datetime


class Game(GameBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)


class Token(SQLModel, table=True):
    token_value: str = Field(primary_key=True)
    user_id: int = Field(foreign_key="user.id")
    