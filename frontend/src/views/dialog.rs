use leptos::*;
use leptos::logging::log;
use leptos::html::Dialog;
use leptos::ev::{Event, MouseEvent};

use crate::views::timer::prettify_time;
use crate::{GlobalState, GameStatus};
use crate::views::login_sigin::Login;
use crate::views::me::Me;
use crate::views::user::User;

#[component]
pub fn Dialog() -> impl IntoView {

    let state = expect_context::<RwSignal<GlobalState>>();

    let (me, update_time) = create_slice(
        state,
        |state| state.me.clone(),
        |state, new_time: u32| {
            match state.me.as_mut() {
                None => (),
                Some(u) => u.best_time = new_time
            }
            // state.me.as_mut().expect("line activated only if logged in").best_time = new_time;
        },
    );

    let (game_status, _) = create_slice(
        state,
        |state| state.status.clone(),
        |_, _: User| (),
    );

    let (time, _) = create_slice(
        state,
        |state| state.time.clone(),
        |_, _: User| (),
    );

    let dialog_el = create_node_ref::<Dialog>();

    let show_dialog = move |_: MouseEvent| {
        match dialog_el.get().expect("dialog has been created").show_modal() {
            Ok(_) => (),
            Err(_) => log!("Unable to open the dialog :, ")
        }
    };

    let close_dialog = move |_: MouseEvent| {
        dialog_el.get().expect("dialog has been created").close();
    };

    let on_close = move |e: Event| {
        e.prevent_default();
    };



    create_effect(move |_| {
        if game_status() == GameStatus::Won {
            match dialog_el.get().expect("dialog has been created").show_modal() {
                Ok(_) => (),
                Err(_) => log!("Unable to open the dialog :, ")
            };
        };
    });


    view! {
        <dialog
        _ref=dialog_el
        on:close=on_close
        >
        {
            move || match (game_status(), me.get()) {
                (GameStatus::Won, Some(u)) => {
                    view! {
                        <div>
                            { if u.best_time as u32 > time() {
                                update_time(time.get());
                                view! { <p> "New Best Time !"</p> }
                            } else {
                                view! { <p/> }
                            }
                        }
                            <p style="margin-top: 15px;"> {format!("You finished the game in {}",prettify_time(time.get()))} </p>
                        </div>
                    }
                },
                (GameStatus::Won, None) => {
                    view! {
                        <div>
                            <p> {format!("You finished the game in {}",prettify_time(time.get()))} </p>
                            <Login />
                        </div>
                    }
                },
                (_, None) => {
                    view! {
                        <div>
                            <Login />
                            <p>"You need to finish one game in order to sign in"</p>
                        </div>
                    }
                },
                (_, Some(u)) => {
                    view! { <div><Me user=u /></div> }
                }
            }
        }

            
            <button on:click=close_dialog class="close-button"/ >
        </dialog>

        <p>
            <button id="showDialog" on:click=show_dialog class="control-button">{move || match me.get() {
                None => "Login",
                Some(_) => "My Account"
            }}</button>
        </p>
    }
}