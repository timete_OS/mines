use crate::game::case::CaseView;
use leptos::*;


#[component]
pub fn CaseContent(case: ReadSignal<CaseView>) -> impl IntoView {

    view! {
        <svg
            viewBox="0 0 1000 1000"
            height="100%"
            preserveAspectRatio="xMidYMid"
            style="background-color:#babdb6"
            xmlns="http://www.w3.org/2000/svg">
            {move || {
                match case() {
                    CaseView::Invisible => view! {<Blanck/>},
                    CaseView::Flag(b) => view! {<Flag valid=b/>},
                    CaseView::Mine(b) => view! {<Mine clicked=b/>},
                    CaseView::Visible(0) => view! {<Zero/>},
                    CaseView::Visible(n) => view! {<Text number=n/>}
                }
            }
        }
        </svg>
    }
}


#[component]
fn Blanck() -> impl IntoView {
    view! {
        <rect width="1000" height="1000" fill="#babdb6" class="svg" />
    }
}

#[component]
fn Zero() -> impl IntoView {
    view! {
        <rect width="1000" height="1000" fill="#dededc" />
    }
}


#[component]
fn Text(number: usize) -> impl IntoView {

    let colors = [
        "#dededc", "#ddfac3", "#ecedbf", "#eddab4", "#edc38a", "#f7a1a2", "#fea785", "#c149e6",
    ];

    let color = colors[number];
    let text = match char::from_digit(number as u32, 10) {
        Some(c) => if c == '0' {'_'} else {c},
        None => panic!("Neighbourghs are supposed to be maxed at 8")
    };

    view! {
        <rect width="1000" height="1000" fill={color} />
        <text x="500" y="700" font-size="700" text-anchor="middle" fill="black">{text}</text>
    }
}


#[component]
fn Flag(valid: bool) -> impl IntoView {
    // if the flag is unvalid, background should be red and flag unchanged
    let background = if valid {"#000000"} else {"red"};

    view! {
        <g class="layer" >
            <path fill={background} d="M804.016,256.745 L266,397 L266,115.500 L804.016,256.745 Z" class="cls-1"/>
            <path fill={background} d="M246.000,91.000 L257.207,91.000 C262.729,91.000 267.207,95.477 267.207,101.000 L267.207,898.000 C267.207,903.523 262.729,908.000 257.207,908.000 L246.000,908.000 C240.477,908.000 236.000,903.523 236.000,898.000 L236.000,101.000 C236.000,95.477 240.477,91.000 246.000,91.000 Z" class="cls-1"/>
        </g>
    }
}


#[component]
fn Mine(clicked: bool) -> impl IntoView {
    // if the mine is clicked it should be red, with background unchanged
    let mine_color = if clicked {"red"} else {"#000000"};

    view! {
        <g class="layer" style="background-color:#888a85">
            <path fill={mine_color} d="M499.46 202.774c164.153 0 297.226 133.072 297.226 297.226S663.613 797.226 499.46 797.226c-164.154 0-297.227-133.072-297.227-297.226S335.306 202.774 499.46 202.774Z" class="cls-1"/>
            <path fill={mine_color} d="M486.49 67.671h27.02v864.658h-27.02V67.671Z" class="cls-1"/>
            <path fill={mine_color} d="m118.837 295.536 13.51-23.401 748.816 432.329-13.51 23.401-748.816-432.329Z" class="cls-1"/>
            <path fill={mine_color} d="m867.653 272.135 13.51 23.401-748.816 432.329-13.51-23.401 748.816-432.329Z" class="cls-1"/>
        </g>
    }
}

