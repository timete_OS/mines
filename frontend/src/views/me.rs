use leptos::*;
use web_sys::MouseEvent;

use crate::views::timer::prettify_time;
use crate::GlobalState;

use crate::views::user::User;


#[component]
pub fn Me(user: User) -> impl IntoView {

    let state = expect_context::<RwSignal<GlobalState>>();

    let (_, reset_id) = create_slice(
        state,
        |state| state.identification.clone(),
        |state, _: usize| state.identification = None
    );

    let (_, reset_me) = create_slice(
        state,
        |state| state.me.clone(),
        |state, _: usize| state.me = None
    );

    let log_out = move |_: MouseEvent| {
        reset_id(0);
        reset_me(0);
    };

    let bt = prettify_time(user.best_time as u32);

    view! {
        <b>{user.name}</b>
        <span class="user">" Your best time is "{bt}</span>

        <button on:click=log_out class="control-button">"Log Out"</button>
    }
}


