use leptos::*;

use gloo_net::http::Request;
use leptos::logging::log;

use crate::api_url;
use crate::views::user::{User, UserView, ReceivedUser};


pub async fn load_users() -> Vec<User> {
    let users_url = concat!(api_url!(), "/sorted/users");

    log!("loading data from API");

    let fetched_users = Request::get(users_url)
        .send()
        .await;

    let b: Result<Vec<ReceivedUser>, gloo_net::Error> = match fetched_users {
        Ok(users) => {
            log!("pas de soucis pour charger les données");
            users.json().await
        }
        Err(e) => Err(e)
    };

    match b {
        Ok(users) => {
            users.iter()
            .filter(|u| u.best_time.is_some())
            .map(|u| User {
                name: u.name.clone(), // here does not make any sense to clone, as I want to consume the original vector
                member_since: u.member_since.clone(),
                best_time:  u.best_time.expect("Problem with the compiler") as u32
            })
            .collect()
        },
        Err(_) => {
            log!("problème pour la dé-serialisation ou pour charger les données");
            vec![]
        }
        
    }
}


#[component]
pub fn Users() -> impl IntoView {

    let once = create_resource(|| (), |_| async move { load_users().await });

    view! {
        <h3 style="text-align: center;">"LEADERBOARD"</h3>
            {move || match once.get() {
                None => view! {<ul></ul>},
                Some(users) => view! {
                <ul type="disc">
                    {users.iter().map(|u| view! {<li><UserView user=u.clone()/></li>})
                    .collect::<Vec<_>>()}
                </ul>
            },
        }}
    }
}