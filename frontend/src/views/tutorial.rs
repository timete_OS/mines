use leptos::{*, html::Dialog, logging::log};
use leptos::ev::{Event, MouseEvent};
use crate::api_url;


#[component]
pub fn Tutorial() -> impl IntoView {

    let dialog_el = create_node_ref::<Dialog>();

    let (is_open, open) = create_signal(false);

    let show_dialog = move |_: MouseEvent| {
        match dialog_el.get().expect("dialog has been created").show_modal() {
            Ok(_) => open.set(true),
            Err(_) => log!("Unable to open the dialog :, ")
        }
    };

    let close_dialog = move |_: MouseEvent| {
        dialog_el.get().expect("dialog has been created").close();
    };

    let on_close = move |e: Event| {
        e.prevent_default();
    };


    let gif_url = concat!(api_url!(), "/tutorial.gif");


    view! {
        <dialog
        _ref=dialog_el
        on:close=on_close
        // on:cancel=close_dialog
        >
        <div class="column">
            <img src={
                move || if is_open() {
                    gif_url
                    // "tutorial.gif"
                } else {
                    ""
                }
            }
                width="600" height="421"/>
        </div>
        <div class="column">
            <h3 style="text-align: center;">Some explanations</h3>
            <ul>
                <li>Every visible case shows the number of adjacent mines</li>
                <li>You do not want to click on a mine</li>
                <li>You can put a flag on a case with a right click, or shift + left click</li>
                <li>If you click on a visible case, it will click on adjacent cases <i>(useful to go fast but can be dangerous)</i></li>
                <li>The goal is to click all cases without a mine</li>
                <li>Good Luck</li>
                </ul>

            <button on:click=close_dialog class="close-button"/ >
        </div>
        </dialog>

        <p>
            <button id="showDialog" on:click=show_dialog class="control-button">"Tutorial"</button>
        </p>
    }
}