use std::{cell::RefCell, rc::Rc};
use leptos::{*, logging::log, html::Dialog};
use leptos::ev::{Event, MouseEvent};
use web_sys::KeyboardEvent;

use crate::{game::grid::Grid, GlobalState, GameStatus};


#[component]
pub fn GameControl(grid: Rc<RefCell<Grid>>, set_flag: WriteSignal<usize>) -> impl IntoView {

    let state = expect_context::<RwSignal<GlobalState>>();

    let (status, set_status) = create_slice(
        state,
        |state| state.status.clone(),
        |state, n| state.status = n,
    );

    let (_, set_timer) = create_slice(
        state,
        |state| state.time.clone(),
        |state, n| state.time = n,
    );

    let dialog_el = create_node_ref::<Dialog>();


    let g= grid.clone();
    let reset = move |_e: MouseEvent| {
        match g.try_borrow_mut() {
            Ok(mut valid_grid_ref) => {
                match status() {
                    GameStatus::Idle => (),
                    GameStatus::Playing => {
                        set_status(GameStatus::Idle);
                        match dialog_el.get().expect("dialog has been created").show_modal() {
                            Ok(_) => (),
                            Err(e) => log!("Unable to open the dialog : {:?}", e)
                        }
                    },
                    _ => {
                        set_status.set(GameStatus::Idle);
                        set_timer(0);
                        set_flag(0);
                        valid_grid_ref.reset();
                    }
                }
            }
            Err(_) => log!("wtf I don't have the grid ????")
        }
    };
    

    let close_dialog = move |_: MouseEvent| {
        set_status(GameStatus::Playing);
        dialog_el.get().expect("dialog has been created").close();
    };

    let on_esc = move |_: KeyboardEvent| {
        set_status(GameStatus::Playing);
    };

    let on_close = move |e: Event| {
        e.prevent_default();
    };


    let reset_for_real = move |_| {
        set_timer(0);
        set_flag(0);
        match grid.try_borrow_mut() {
            Ok(mut valid_grid_ref) => {
                valid_grid_ref.reset()
            },
            Err(_) => log!("wtf I don't have the grid ????")
        }
        dialog_el.get().expect("dialog has been created").close();
    };


    let eset = reset.clone();
    let handle = window_event_listener(ev::keydown, move |ev| {
        if ev.alt_key() && ev.code() == "KeyR" {
            match MouseEvent::new("needed") {
                Ok(event) => eset(event),
                Err(e) => log!("there has been some error creating the mouse event {:?}", e)
            }
        }
    });

    on_cleanup(move || handle.remove());

    

    view!{
        <dialog
        _ref=dialog_el
        on:close=on_close
        on:cancel=on_esc
        class="dialog"
        >
            <p><b>"Are you sure you want to start a new game ?"</b></p>
            <button value="cancel" on:click=close_dialog class="control-button">"Cancel"</button>
            <button value="reset" on:click=reset_for_real class="control-button">"Start New Game"</button>
        </dialog>

        <button on:click=reset class="control-button">{ 
            move || match status.get() {
                GameStatus::Playing => "Start Over",
                _ => "Play Again"
            }
        }</button>
    }
}