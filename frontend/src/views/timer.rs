use leptos::*;
use gloo_timers::future::TimeoutFuture;

use crate::{GlobalState, GameStatus};


pub fn prettify_time(time: u32) -> String {
    let (minutes, seconds) = (time as usize / 60, time as usize % 60);

    match (minutes, seconds) {
        (2.., 2..) => format!("{minutes} minutes {seconds} seconds"),
        (1, 2..) => format!("1 minute {seconds} seconds"),
        (2.., 1|0) => format!("{minutes} minutes {seconds} second"),
        (0, _) => format!("{seconds} seconds"),
        (1, 1|0) => format!("1 minute {seconds} second"),
        // (_, _) => panic!("fuk me im sure i am exhaustive here !")
    }
}

#[component]
pub fn Timer() -> impl IntoView {

    let state = expect_context::<RwSignal<GlobalState>>();

    let (time, set_time) = create_slice(
        // we take a slice *from* `state`
        state,
        // our getter returns a "slice" of the data
        |state| state.time,
        // our setter describes how to mutate that slice, given a new value
        |state, n| state.time = n,
    );


    let (game_status, _) = create_slice(
        state,
        |state| state.status.clone(),
        |state, n| state.status = n,
    );

 
    let pass_time = create_action(|current_time: &(u32, SignalSetter<u32>)| {
        let (time, time_setter) = current_time.to_owned();
        async move {
            // wainting a second
            TimeoutFuture::new(1_000).await;
            time_setter(time + 1);
        }
    });

    create_effect(move |_: Option<Signal<u32>>| {
        if game_status() == GameStatus::Playing {
           pass_time.dispatch((time(), set_time));
        }
        
        time
    });

    let formatted_time = move || {
        let s = time.get();
        let (mut minutes, seconds) = (s / 60, s % 60);
        if minutes > 59 {
            let hours = minutes / 60;
            minutes %= 60;
            return format!("{hours:0>2}:{minutes:0>2}:{seconds:0>2}")
        }
        format!("{minutes:0>2}:{seconds:0>2}")
    };

    view! {
        <div class="timer">
            <div>
                <svg width="100%"
                viewBox="-2.4 -2.4 28.8 28.8"
                preserveAspectRatio="xMidYMid"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                stroke="#000"
                stroke-width="0">
                    <g stroke-linecap="round" stroke-linejoin="round"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M12 2.75a9.25 9.25 0 1 0 0 18.5 9.25 9.25 0 0 0 0-18.5ZM1.25 12C1.25 6.063 6.063 1.25 12 1.25S22.75 6.063 22.75 12 17.937 22.75 12 22.75 1.25 17.937 1.25 12ZM12 7.25a.75.75 0 0 1 .75.75v3.69l2.28 2.28a.75.75 0 1 1-1.06 1.06l-2.5-2.5a.75.75 0 0 1-.22-.53V8a.75.75 0 0 1 .75-.75Z" fill="#1C274C" stroke="none"/>
                </svg>
            <div> {formatted_time} </div>
        </div>
              // <button
        //     on:click=move |_| {
        //         match time_status()
        //         {
        //             Status::Running => set_status(Status::Idle),
        //             _ => set_status(Status::Running)
        //         }
        //     }
        // > " Start/Stop "
        // </button>

        // <button
        // on:click=move |_| {
        //     set_status(Status::Reset);
        // }
        // > " Reset "
        // </button>

        </div>
    }
}