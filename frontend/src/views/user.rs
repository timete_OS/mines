use leptos::*;
use leptos::logging::log;
use leptos::IntoView;
use gloo_net::http::Request;


use serde::{Deserialize, Serialize};

use crate::api_url;
use crate::views::timer::prettify_time;

#[derive(Clone, PartialEq, Default, Debug, Deserialize, Serialize)]
pub struct User {
    pub name: String,
    pub member_since: String,
    pub best_time: u32
}

#[derive(Clone, PartialEq, Deserialize, Default, Debug, Serialize)]
pub struct ReceivedUser {
    pub name: String,
    pub member_since: String,
    pub id: usize,
    pub best_time: Option<u64>
}

pub async fn load_user(id: usize, time: u32) -> Option<User> {
    let id_string = format!("/user/{}", id);
    let url = api_url!().to_string() + id_string.as_str();

    let fetched_user = Request::get(&url)
        .send()
        .await;

    let parsed_response: Result<ReceivedUser, gloo_net::Error> = match fetched_user {
        Ok(user) => user.json().await,
        Err(e) => Err(e)
    };

    match parsed_response {
        Ok(user) => Some(User {
            name: user.name,
            member_since: user.member_since,
            best_time: match user.best_time {
                Some(t) => t as u32,
                None => time
            }
        }),
        Err(e) => {
            log!("problème pour la dé-serialisation, {:?}", e);
            None
        }
        
    }
}


#[component]
pub fn UserView(user: User) -> impl IntoView {

    let bt = prettify_time(user.best_time);

    view! {
        <div class="user"><b>{user.name}</b></div>
        <div class="times"><span><i>{bt}</i></span></div>
    }
}


