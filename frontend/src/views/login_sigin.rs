use leptos::*;
use leptos::html::Input;
use leptos::ev::SubmitEvent;
use leptos::logging::log;

use crate::network::userland::{Identification, send_credentials, Message, ServerResponse};
use crate::{GlobalState, GameStatus};
use crate::views::user::{load_user, User};




#[component]
pub fn Login() -> impl IntoView {

    let state = expect_context::<RwSignal<GlobalState>>();


    // lol at this point I should just use directly the state instead of creating slices
    let (_, set_id) = create_slice(
        state,
        |state| state.identification.clone(),
        |state, n: Identification| state.identification = Some(n),
    );

    let (_, set_me) = create_slice(
        state,
        |state| state.me.clone(),
        |state, n: User| state.me = Some(n),
    );

    let (status, _) = create_slice(
        state,
        |state| state.status.clone(),
        |state, t| state.status = t,
    );

    let (time, _) = create_slice(
        state,
        |state| state.time.clone(),
        |_, _: &()| (),
    );

    let (name, _) = create_signal("Username".to_string());
    let (password, _) = create_signal("Password".to_string());


    let send_data = create_action(|input: &(String, String, Message)| {
        let (name, password, action) = input.to_owned();
        async move { send_credentials(name, password, action).await }
    });


    let name_element: NodeRef<Input> = create_node_ref();
    let pswd_element: NodeRef<Input> = create_node_ref();

    let on_submit = move |ev: SubmitEvent| {
        // stop the page from reloading!
        ev.prevent_default();

        let action = match ev.submitter().expect("il y a bien un truc").id().as_str() {
            "login" => Message::Login,
            _ => Message::SignIn(time.get())
        };

        // here, we'll extract the value from the input
        let name = name_element().expect("<input> to exist").value();
        let pswd = pswd_element().expect("password exists").value();
        log!("Send request with name : {name} and password : ******");
        send_data.dispatch((name, pswd, action));
    };

    let fetch_me = create_action(|input: &(usize, u32)| {
        let (id, time) = *input;
        async move { load_user(id, time).await }
    });

    let (server_response, there_is_an_error) = create_signal(ServerResponse::Valid);

    // this triggers when we get the token and id from the server
    create_effect(move |_| {
        // send_data.
        match send_data.value()() {
            Some(s) =>  {
                match s {
                    Ok(id) => {
                        let user_id = id.user_id;
                        set_id(id);
                        fetch_me.dispatch((user_id, time()));
                    },
                    Err(e) => there_is_an_error.set(e)
                }
            },
            None => log!("server has crashed ?") // default state, this will run on startup
        }
    });

    // this triggers when we get the user from the server
    create_effect(move |_| {
        match fetch_me.value()() {
            Some(u) =>  {
                let u = u.unwrap();
                set_me(u);
            },
            None => log!("wrong response from the server")
        }
    });



    view! {

        // <div class="container">
        // <form class="form">
        //     <h2 class="form-title">Login</h2>
        //     <div class="input-group">
        //         <label for="username">Username</label>
        //         <input type="text" id="username" name="username" required>
        //     </div>
        //     <div class="input-group">
        //         <label for="password">Password</label>
        //         <input type="password" id="password" name="password" required>
        //     </div>
        //     <button type="submit" class="btn">Login</button>
        // </form>
        // </div>
        <p style="color: red; text-align: center;">"You are not logged in"</p>
        <form on:submit=on_submit class="form">

        <div class="input-group">
                <input type="text"
                        // id="username"
                        // name="username"
                        placeholder=name
                        node_ref=name_element
                        class="input"
                        required
                        />
        </div>
        <div class="input-group">
                <input type="password"
                        // id="password"
                        // name="password"
                        placeholder=password
                        node_ref=pswd_element
                        class="input"
                        required
                        />
        </div>

           
            // <input type="text"
            //     // here, we use the `value` *attribute* to set only
            //     // the initial value, letting the browser maintain
            //     // the state after that
            //     placeholder=name

            //     // store a reference to this input in `name_element`
            //     node_ref=name_element
            //     class="input"

            // />

            // <input type="password"
            //     placeholder=password
            //     node_ref=pswd_element
            //     class="input"
            // />


        // <button type="submit" class="btn">Login<

            <div>
                <input type="submit" value="Log In" id="login" class="control-button" />
            </div>
            {move || match status.get() {
                GameStatus::Won => view! { 
                    <div>
                        <input type="submit" value="Sign In" id="signin" class="control-button" style="margin-top: 15px;"/>
                    </div> },
                _ => view! { <div/> },
            }}
            <div>
            {
                move || match server_response.get() {
                    ServerResponse::Valid => view! {<p/>},
                    ServerResponse::Crashed => view! {<p style="color: red;">"Congratulation ! You just crashed the server !"</p>},
                    ServerResponse::NotFound => view! {<p style="color: red;">"Ressources not found."</p>},
                    ServerResponse::WrongAuth => view! {<p style="color: red;">"Sorry, name or password is invalid :-("</p>}
                }
            }
            </div>
        </form>
    }
}
