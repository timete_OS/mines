use std::cell::RefCell;
use std::rc::Rc;

use leptos::*;
use leptos::logging::log;
use web_sys::MouseEvent;
use crate::{GlobalState, GameStatus};
use crate::game::grid::Grid;
use crate::views::case::CaseContent;


#[component]
pub fn Grid(grid: Rc<RefCell<Grid>>, set_flag: WriteSignal<usize>) -> impl IntoView {

    let state = expect_context::<RwSignal<GlobalState>>();

    let (game_status, set_status) = create_slice(
        state,
        |state| state.status.clone(),
        |state, n| state.status = n,
    );

    let cases = (*grid).borrow().content.iter().flatten().map(|case| {

        let coordinates = case.coordinates.clone();

        let g = grid.clone();
        let onclick = move |e: MouseEvent| {
            let shift_pressed = e.shift_key();
            match g.try_borrow_mut() {
                Ok(mut valid_grid_ref) => {
                    match (game_status(), shift_pressed) {
                        (GameStatus::Idle, false) => {
                            set_status(GameStatus::Playing);
                            valid_grid_ref.initialize(coordinates);
                        }
                        (GameStatus::Playing, false) => valid_grid_ref.click(coordinates),
                        (GameStatus::Playing, true) => valid_grid_ref.right_click(coordinates),
                        (GameStatus::Idle, true) => valid_grid_ref.right_click(coordinates),
                        _ => ()
                    }
                },
                Err(_) => log!("oupsi, you clicked on two cases at the same time ?")
            }

        };

        let g = grid.clone();
        let onrightclick = move |e: MouseEvent| {
            e.prevent_default();
            match g.try_borrow_mut() {
                Ok(mut valid_grid_ref) => {
                    match game_status() {
                        GameStatus::Idle => valid_grid_ref.right_click(coordinates),
                        GameStatus::Playing => valid_grid_ref.right_click(coordinates),
                        _ => ()
                    }
                    set_flag(valid_grid_ref.flag_count);
                },
                Err(_) => log!("oupsi")
            }
        };

        view! {
            <button on:click=onclick on:contextmenu=onrightclick class="case">
                <CaseContent case={case.state}/>
            </button>
        }
    }
    ).collect_view();

    view! {
        <div class="grid-container">
            {cases}
        </div>
    }
}