use leptos::*;


#[component]
pub fn FlagCount(count: ReadSignal<usize>, total: usize) -> impl IntoView {

    view!{
        <div class="timer">
            <div>
                <svg
                viewBox="0 0 1000 1000"
                preserveAspectRatio="xMidYMid"
                width="100%">
                    <path fill="black" d="M804.016,256.745 L266,397 L266,115.500 L804.016,256.745 Z" class="cls-1"/>
                    <path fill="black" d="M246.000,91.000 L257.207,91.000 C262.729,91.000 267.207,95.477 267.207,101.000 L267.207,898.000 C267.207,903.523 262.729,908.000 257.207,908.000 L246.000,908.000 C240.477,908.000 236.000,903.523 236.000,898.000 L236.000,101.000 C236.000,95.477 240.477,91.000 246.000,91.000 Z" class="cls-1"/>
                </svg>
            </div>
            <p> {count} / {total} </p>
        </div>
    }
}