mod game;
mod views;
mod network;


use std::cell::RefCell;
use std::rc::Rc;

use leptos::*;
use leptos::logging::log;


use crate::game::grid::Grid;
use crate::network::gameland::put_game;
use crate::views::flag_count::FlagCount;
use crate::views::game_control::GameControl;
use crate::views::grid::Grid;
use crate::views::user::User;
use crate::network::userland::Identification;
use crate::views::timer::Timer;
use crate::views::users::Users;
use crate::views::dialog::Dialog;
use crate::views::tutorial::Tutorial;


#[macro_export]
#[cfg(not(debug_assertions))]
macro_rules! api_url {
    () => ("https://thispainofmine.com")
}


#[macro_export]
#[cfg(debug_assertions)]
macro_rules! api_url {
    () => ("/api")
}


#[derive(Default, Clone, Debug, PartialEq)]
pub enum GameStatus {
    #[default]
    Idle,
    Playing,
    Lost,
    Won
}

#[derive(Clone, Debug, Default)]
pub struct GlobalState {
    pub time: u32,
    pub status: GameStatus,
    pub identification: Option<Identification>,
    pub me: Option<User>
}


fn main() {
    let b = leptos::document().body().unwrap();
    let children = b.child_nodes();
    for i in (0..children.length()).rev() {
        match children.get(i) {
            Some(node) =>  {
                log!("there is a child node {}", node.node_name());
                // let text = node.id
                match b.remove_child(&node) {
                    Err(e) => log!("error in removing node, {:?}", e),
                    Ok(node) => log!("node {} was removed as expected", node.node_name())
                }
            },
            None => ()
        }
    }
    
    leptos::mount_to_body(|| view! { <App/> })
}

#[component]
fn App() -> impl IntoView {
    let dimensions = (16, 30);
    let mut nmines = 99;
    let grid = Grid::new(dimensions, nmines);
    nmines = grid.n_mines;

    provide_context(create_rw_signal(GlobalState::default()));

    // Every case must be able to access the grid, so I put it behind a RefCell
    let grid_ref = Rc::new(RefCell::new(grid));

    
    // I shoud add it to the state struct latter on
    let (flag_count, set_count) = create_signal(0);


    let state = expect_context::<RwSignal<GlobalState>>();
    let (id, _) = create_slice(state, |state| state.identification.clone(), |_state, _: &()| ());
    let (status, _) = create_slice(state, |state| state.status.clone(), |_state, _: &()| ());
    let (time, _) = create_slice(state, |state| state.time.clone(), |_state, _: &()| ());
    // let (me, update_best_time) = create_slice(
    //     state,
    //     |state| state.me.clone(),
    //     |state, new_best_time: u32| state.me.expect("user to be set").best_time = new_best_time as f64
    // );


    let post_game = create_action(move |input: &(String, usize, u32)| {
        let (token, id, time) = input.to_owned();
        async move {put_game(token, id, time, nmines).await }
    });


    // Will triger and send data to the server when the user win the game
    let _ = watch(
        move || (id.get(), status.get()),
        move |(id, status), _, _| {
        if *status == GameStatus::Won && id.is_some() {
            let id = id.as_ref().unwrap();
            let (token, player_id) = (id.token_value.clone(), id.user_id);
            post_game.dispatch((token, player_id, time.get()));
            // here I need to update the local value of best time if necessary
        }
    },
        false
    );

    view! {
        <div class="box">
            <div class="left">
                <Grid grid=grid_ref.clone() set_flag=set_count/>
            </div>
            <div class="right">
                <Timer />
                <FlagCount count=flag_count total=nmines />
                <div class="leaderboard">
                    <Users />
                </div>
                <div class="control-container">
                    <GameControl grid=grid_ref set_flag=set_count/>
                    <Dialog />
                    <Tutorial />
                    <a href="https://gitlab.com/timete_OS/mines" target="_blanck"><p>"❤️ Contribute"</p></a>
                </div>
            </div>
        </div>
    }
}
