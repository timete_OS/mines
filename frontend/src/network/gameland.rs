use leptos::logging::log;
use rand::{thread_rng, Rng};
use std::str;

use base64::{engine::general_purpose, Engine as _};


use serde::{Deserialize, Serialize};
use gloo_net::http::Request;

use crate::api_url;

#[derive(Default, Debug, Clone, PartialEq, Deserialize)]
pub struct Identification {
    pub user_id : usize,
    pub token_value: String
}

#[derive(Clone,PartialEq, Debug,Serialize,Deserialize)]
struct Game {
    pub player_id: usize,
    pub time: u32,
    pub mines_number: usize
}

const PERMUTATION_BOX: [u8; 256] = [255, 2, 3, 100,  64, 228,  34, 214, 233,  94, 108, 114, 152,
71,  40,   123,  83, 150, 120, 177,  68, 176, 209, 170, 200,  91,
127, 119, 153,   9, 215, 174,  44,  53, 131, 187,  48, 185, 102,
82, 208, 230, 243,  29, 201,  45,   6,  62,  38, 135,  63, 222,
219,  86,  32, 178, 159,  93,  31,  28,  67, 139, 193, 164,   4,
84, 229, 205, 239,  66,  36, 191,  58, 148,  79, 195, 107,  24,
138, 251,  98,  27, 121,  20, 105, 236,  77, 155,  26, 145,  51,
39, 190, 168, 126, 237, 223, 157, 154,  75, 244, 172, 192, 133,
122, 202,  19,  33, 130, 181, 216, 169, 87, 249,  80,  54, 204,
74,  70,  96, 188, 171, 218, 101, 182, 186, 203,  52,  95,  76,
254, 207, 147,   8,  42,  65,  23, 140,   7, 211, 194, 158,   248,
16,  41, 212,  78,  22,  61, 180, 104,   163,  50,  15, 240, 117,
128, 247, 189, 166, 206, 134,  47,  37, 132, 198, 160, 226, 115,
231, 253, 141, 109, 111,  55, 161,  10, 146,  13,  59,  60,  25,
99,  17,  43, 110, 199, 238, 245, 136,  85, 224, 234, 142, 235,
173, 210,  69,  81,  18,  89, 103, 225, 184, 232,  73, 183, 241,
11, 113, 246,   5, 116, 162, 197,  12,  49, 118, 143, 129,  57,
90,  14, 165, 124, 156,   1, 112,  35,  21, 137, 217,  97, 149,
175, 106,  92, 144, 125, 179, 242,  88, 196,  30,  46, 252, 151,
227, 167,  56, 250,  72, 220, 221, 213, 0];

fn chiffre(content: Vec<u8>, key: &[u8]) -> Vec<u8> {
    let shift_box: Vec<u8> = (0..=255).map(|x: u8| x.overflowing_add(7).0).collect();

    let mut block = content.to_owned();
    for _ in 0..16 {
        block = block.iter().map(|&x| shift_box[x as usize]).collect();
        block.iter_mut().zip(key.iter()).for_each(|(x, y)| *x = PERMUTATION_BOX[(*x ^ y) as usize]);
    }

    block
}

pub async fn put_game(token: String, player_id: usize, time: u32, mine_number: usize) -> bool {

    let put_url = concat!(api_url!(), "/game");
    let mut rng = thread_rng();

    let key = match option_env!("INTERFACE_KEY") {
        Some(v) => {
            log!("Using environement key");
            v.as_bytes()
        },
        // None => "zPh6RrLsvUrXcGSHmZZWVZc75QWyU8Dt".as_bytes().to_owned()
        None => "FvbvMVpjGE49DJ3ivihJJYzbozALoVcZ".as_bytes()
    };

    let mut block = [0u8; 32];
    rng.fill(&mut block); // 128 random bits

    let h = token[..16].as_bytes(); // first 128 bits from the token
    for (index, b) in h.iter().enumerate() {
        block[index] = *b;
    }

    let passcode = chiffre(block.to_vec(), &key);
    let passcode = general_purpose::STANDARD.encode(&passcode);


    let maybe_correct_response = Request::put(put_url)
        // .header("Content-Type", "application/json")
        // .headers("token", token)
        .header("token", &token)
        .header("passcode", &passcode)
        .json(&Game {
            player_id: player_id,
            time: time,
            mines_number: mine_number
        })
        .expect("could not build the request")
        .send()
        .await;

    match maybe_correct_response {
        Ok(_) => {
            // log!("{:?}", response);
            true
        },
        Err(e) => {
            log!("{:?}", e);
            false
       }
    }
}

