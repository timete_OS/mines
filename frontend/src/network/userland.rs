use leptos::logging::log;
use serde::{Deserialize, Serialize};
use gloo_net::http::Request;

use crate::api_url;

#[derive(Clone)]
pub enum ServerResponse {
    Valid,
    WrongAuth,
    NotFound,
    Crashed
}

#[derive(Default, Debug, Clone, PartialEq, Deserialize)]
pub struct Identification {
    pub user_id : usize,
    pub token_value: String
}

#[derive(Clone,PartialEq, Debug, Serialize, Deserialize)]
struct Params {
    pub name: String,
    pub pswd: String,
    pub time: u32
}

#[derive(Clone, PartialEq, Debug ,Deserialize)]
struct WrongAuth {
    detail: String
}

#[derive(Clone)]
pub enum Message {
    Login,
    SignIn(u32)
}

pub async fn send_credentials(username: String, password: String, m: Message) -> Result<Identification, ServerResponse> {

    let login_url = concat!(api_url!(), "/login");
    let signin_url = concat!(api_url!(), "/user");

    let maybe_fetched_credentials = match m {
        Message::Login => {
            Request::get(login_url)
            .header("Content-Type", "application/json")
            .header("name", username.as_str())
            .header("pswd", password.as_str())
            // .query([("name", username), ("pswd", password)])
            // .expect("Could not build that request.")
            .send()
            .await
        },
        Message::SignIn(time) => {
            Request::put(signin_url)
            // .body(serde_wasm_bindgen::to_value(&(*me)).unwrap())
            .json(&Params {
                name: username,
                pswd: password,
                time: time
            })
            .expect("The server returned an error")
            .send()
            .await
        }
    };

    log!("request sent");
    
    match maybe_fetched_credentials {
        Err(_) => Err(ServerResponse::Crashed),
        Ok(response) => {
            match response.status() {
                404 => Err(ServerResponse::NotFound),
                401 => Err(ServerResponse::WrongAuth),
                200 => match response.json().await {
                        Ok(json_response)=> Ok(json_response),
                        Err(_) => {
                            let h: Result<WrongAuth, _> = response.json().await;
                            log!("trying to parse the error");
                            match h {
                                Ok(details) => log!("{}", details.detail),
                                Err(e) => log!("{:?}", e)
                            };
                            Err(ServerResponse::Crashed)
                        }
                    }
                _ => Err(ServerResponse::Crashed)
                }
            }
    }
}