use leptos::*;


#[derive(Clone, PartialEq, Debug)]
pub enum CaseView {
    Invisible,
    Visible(usize),
    Flag(bool),
    Mine(bool)
}




#[derive(Clone, PartialEq, Debug)]
pub struct Case {
    pub visible: bool,
    pub mine: bool,
    pub neighbours: usize,
    pub flag: bool,
    pub coordinates: (usize, usize),
    pub state: ReadSignal<CaseView>,
    pub set_state: WriteSignal<CaseView>,
}



impl Case {

    pub fn new(coordinates: (usize, usize)) -> Case {

        let (jh, set_jh) = create_signal(CaseView::Invisible);

        Self{
            visible: false,
            mine: false,
            neighbours: 0,
            flag: false,
            coordinates: coordinates,
            state: jh,
            set_state: set_jh,
        }
    }

    pub fn put_mine(&mut self)
    {
        self.mine = true;
    }

    pub fn toggle_flag(&mut self) -> Option<bool>
    {
        // visible cases must remains with flag=false for counting purpose
        if !self.visible
        {
            self.flag = !self.flag;
            let new_view = match self.flag {
                true => CaseView::Flag(true),
                false => CaseView::Invisible
            };
            self.set_state.set(new_view.clone());
            Some(self.flag)
        }
        else {
            None
        }
    }

    pub fn set_neighbours(&mut self, neighbours: usize)
    {
        self.neighbours = neighbours;
    }

    pub fn reveal(&mut self)
    {
        self.visible = true;
        self.flag = false;
        let new_view = match (self.neighbours, self.mine) {
            (t, false) => CaseView::Visible(t),
            (_, true) => CaseView::Mine(false)
        };
        self.set_state.set(new_view.clone());
    }

    pub fn reset(&mut self) {
        self.visible = false;
        self.mine = false;
        self.neighbours = 0;
        self.flag =false;
        self.set_state.set(CaseView::Invisible);
    }
}

impl std::fmt::Display for Case {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        if self.visible
        {
            write!(f, "{}", self.neighbours)
        }
        else {
            write!(f, "H")
        }
   
    }
}

impl std::fmt::Display for CaseView {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            CaseView::Flag(_) => write!(f, "drapeau"),
            CaseView::Invisible => write!(f, "blanck"),
            CaseView::Mine(_) => write!(f, "mines"),
            CaseView::Visible(t) => write!(f, "visible {}", t)
        }
    }
}
