use std::cmp;

use leptos::{expect_context, RwSignal, create_slice};
use leptos::{logging::log, SignalSet};
use rand::thread_rng;
use rand::seq::SliceRandom;
use itertools::Itertools;

use crate::game::case::{Case, CaseView};
use crate::{GameStatus, GlobalState};


#[derive(Clone, PartialEq)]
pub struct Grid {
    pub dimensions: (usize, usize),
    pub n_mines: usize,
    pub content: Vec<Vec<Case>>,
    pub flag_count: usize,
    pub visible_count: usize,
}


impl Grid {

    pub fn new(dimensions: (usize, usize), n_mines: usize) -> Grid {
        let (n, m) = dimensions;
  
        let mines = (0..n).map(|i| {
            (0..m).map(|j| Case::new((i, j))).collect()
        }).collect();

        let grid = Self {
            dimensions: dimensions,
            n_mines: cmp::min(n_mines, n*m - 9),
            content: mines,
            flag_count: 0,
            visible_count: 0
        };

        grid
    }


    fn get_surrounding(&self, point: (usize, usize)) -> itertools::Product<std::ops::Range<usize>, std::ops::Range<usize>>
    {
        let (i, j) = point;
        let (n, m) = self.dimensions;

        let a = i.saturating_sub(1);
        let b = cmp::min(n, i + 2);
        let c = j.saturating_sub(1);
        let d = cmp::min(m, j + 2);

        let it = (a..b).cartesian_product(c..d);

        it
    }



    fn generate_mines(&mut self, point: (usize, usize))
    {
        let (n, m) = self.dimensions;
        let (x, y) = point;
        
        let size = n*m - 1;
    
        let mut vec: Vec<usize> = (0..size).collect();
        let mut rng = thread_rng();
        vec.shuffle(&mut rng);
        
        // initialize the mines with random values
        // makes sure no mines is near the first click
        let mut index = 0;
        let mut n_mines = self.n_mines;
        while n_mines != 0 && index < size
        {
            let e = vec[index];
            let i = e/m;
            let j = e%m;
            // need to find something less ugly
            let is_far_of_first_click = (x as isize - i as isize).abs() > 1 || (y as isize - j as isize).abs() > 1;
            // let b = (i, j) in surrouding;
            if is_far_of_first_click
            {
                (self.content[i][j]).put_mine();
                n_mines -= 1;
            }
            index += 1;
        }
    }

    fn count_mines(&mut self)
    {
        let (n, m) = self.dimensions;

        for j in 0..m {
            for i in 0..n {
                let surrounding = self.get_surrounding((i, j));
                let mut s: usize = 0;
                for (x, y) in surrounding
                {
                    if self.content[x][y].mine
                    {
                        s += 1;
                    }
                }
                self.content[i][j].set_neighbours(s);
            }
        }
    }
    
    pub fn endgame(&mut self, clicked_coordinates: (usize, usize))
    {
        let (i, j) = clicked_coordinates;

        let state = expect_context::<RwSignal<GlobalState>>();

        let (_, set_status) = create_slice(
            state,
            |state| state.status.clone(),
            |state, n| state.status = n,
        );


        set_status.set(GameStatus::Lost);
        
        log!("Game is over !!!!");

        for e in self.content.iter_mut().flatten()
        {
            match (e.mine, e.flag) {
                (false, true) => e.set_state.set(CaseView::Flag(false)),
                (true, false) => e.set_state.set(CaseView::Mine(false)),
                (true, true) => (),
                (false, false) => ()
            }
        }
        self.content[i][j].set_state.set(CaseView::Mine(true));

    }

    pub fn initialize(&mut self, coordinates: (usize, usize))
    {
        self.generate_mines(coordinates);
        self.count_mines();
        self.click(coordinates);
        // weird but I want to make sure that nmines is always the correct ammount of mine on the grid
        self.n_mines = self.content.iter().flatten().fold(0, |x, y| if y.mine { x + 1} else { x });
    }


    pub fn click_neighbours(&mut self, coordinates: (usize, usize))
    {
        let surrouding = self.get_surrounding(coordinates);
        for (i, j) in surrouding
        {
            if !self.content[i][j].visible
            {
                self.click((i, j));
            }
        }

    }


    pub fn click(&mut self, coordinates: (usize, usize))
    {
        let (i, j) = coordinates;
        let (n, m) = self.dimensions;

        match self.content[i][j]
        {
            Case {visible : true, ..} => {
                let surrouding = self.get_surrounding(coordinates);
                let flag_nearby = surrouding.map(|(x, y)| self.content[x][y].flag).fold(false, |x, y| x | y);
                if flag_nearby { self.click_neighbours(coordinates) };
            },
            Case {mine : true, flag : false, ..} => self.endgame(coordinates),
            Case {visible : false, flag : false, neighbours : 0, ..} => {
                self.content[i][j].reveal();
                self.visible_count += 1;
                self.click_neighbours(coordinates);
            },
            Case {mine : false, flag : false, .. } => {
                self.content[i][j].reveal();
                self.visible_count += 1;
            },
            Case {..} => ()
        }

        // check if game is over
        if self.visible_count == (n*m - self.n_mines)
        {
            let state = expect_context::<RwSignal<GlobalState>>();

            let (_, set_status) = create_slice(
                state,
                |state| state.status.clone(),
                |state, n| state.status = n,
            );
    
            set_status.set(GameStatus::Won);
        }
    }


    pub fn right_click(&mut self, coordinates: (usize, usize))
    {
        let (i, j) = coordinates;
        match self.content[i][j].toggle_flag()
        {
            Some(true) => self.flag_count += 1,
            Some(false) => self.flag_count -= 1,
            None => ()
        }
    }


    pub fn reset(&mut self) {

        for e in self.content.iter_mut().flatten() {
            e.reset();
        }
        self.flag_count = 0;
        self.visible_count = 0;

    }

    // pub fn get_probas(&self) -> Vec<Vec<f64>> {

    //     let (i, j) = self.dimensions;
    //     let base_proba = 1. / self.n_mines as f64;
    //     let mut probas = vec![vec![base_proba; j]; i];

    //     let valid_cases_iter = self.content.windows(3)
    //         .flatten()
    //         .filter(|case| case.visible && case.neighbours > 0);

    //     for v in valid_cases_iter {

    //     }
    //     probas     
    // }
}

