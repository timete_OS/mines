Stuff to do 
==========


### TODO

- Mine and Flag icons are honestly bad looking, I need to change this
- When login show a message if wrong identification / internal server error
- Indicate in a little green text that the user is logged in on the main page (?)
- Bad rendering in Chrome

### When developping

use this command if you have also the backend running on port 3000. For this, just go the the **backend/** directory and run `spin up`. Then you can start developing the front end with :

```sh
trunk serve --proxy-backend=http://localhost:3000/ --proxy-rewrite=/api
```



### Used tutorials

- [how to host a static website on S3](https://channaly.medium.com/how-to-host-static-website-with-https-using-amazon-s3-251434490c59) # deprecated (hosting with spin)
- [generating the CI/CD to upload to S3](https://about.gitlab.com/blog/2023/03/01/how-to-deploy-react-to-amazon-s3/) # depredated (hosting with spin)
- leptos doc in general


### WASM Size



50 Mo when developping, but who cares.

- 2 Mo with `trunk build --release`
- 630 Ko with added release profile in **Cargo.toml**
- 420 Ko with added **.cargo/config.toml** to optimise standard lib

maybe not necessary to go under 600k because the compressed 620 Ko is xxx Ko and the compressed 420 Ko is... 160 Ko

so all in all it means I don't have to bother to compress the std lib ? it remove some config files and save some complexity. Why not ?