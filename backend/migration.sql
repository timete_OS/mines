CREATE TABLE IF NOT EXISTS user 
    (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT NOT NULL,
        member_since DATE NOT NULL,
        best_time TIMESTAMP NOT NULL,
        hashed_pswd TEXT NOT NULL
    );

CREATE TABLE IF NOT EXISTS game
    (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        date TIMESTAMP NOT NULL,
        player_id INTEGER NOT NULL,
        time TIMESTAMP NOT NULL,
        mines_number INTEGER NOT NULL,
        FOREIGN KEY (player_id) REFERENCES user(id)
    );

CREATE TABLE IF NOT EXISTS token
    (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        user_id INTEGER NOT NULL,
        token_value TEXT NOT NULL,
        creation_date TIMESTAMP NOT NULL,
        FOREIGN KEY (user_id) REFERENCES user(id)
    );


-- DELETE FROM user;

INSERT INTO user (name, member_since, best_time, hashed_pswd)
VALUES
    ('Timo', date('now'), unixepoch('1970-01-01 00:02:09'), '7092fd8d6c7051cefb3e18246579594f7fdd753a8bf8b843c2e6fcd065673784'),
    ('4rtus', date('now'), unixepoch('1970-01-01 00:03:23'), 'afc522d81b62e36ba0b5e0d5b496c36a6e00bf5fa727fb5e3763f4596108b2ba')
    ;


    -- do0kq2j8lh6se.cloudfront.net.