This Pain Of Mine
============



## General Information

This repo is where lies the code behind the mightiest mining webiste, [thispainofmine.com](https://thispainofmine.com). I encourage you to try it.

## Overall Architecture

I first made the website using [Yew](https://yew.rs/), but then for "learning and having fun" purposes I decided to burn it all down and re-do everything using [Leptos](https://leptos.dev/).

This is a simple static website hosted on S3, as light and simple as possible. It will fetch only (I don't even know if this is good or bad) 160 Ko of compressed data, and should load fast whatever your connection. There is no server side rendering or anything fancy, as it is not required and I know neither how this works nor if this is appropriate.

Given the source files weights around 100 Ko, I guess shipping 160 Ko of wasm is not that bad.

It is snappy enough for a game of minesweeper (this also I am not sure of, as I tried to measure it but I have no idea what to measure).

There is a simple web service to keep track of users and their best scores. It was powered by some Python container hosted on Railway. But as I exhausted my railway credits, I rewrote it in Rust and hosted it on fermyon cloud. Every endpoint is a .wasm module that is loaded when called. This way I get no cold start and no charge when there is no usage. I plan to rewrite this in Haskell, also for purpose of "having fun". Ah. Ah. Ah.

## Contribute

It is almost certain **(p=1)** that my code is full of malpractices and anti-patterns, if there are any patterns at all. Also I despise CSS, so the website consequently looks a bit disgracious, to say the least.

So it is likely you will do a better job than me at almost anything and any of your suggestions are welcome without any form of protocol.

## How to prevent easy cheating

The interface and the server both share a private key. Every game sent to the server are signed using this private key. For now i use a function too simple to be secure, but I intend to change it to something highly non linear. I made this into a seperate project, [aes-light](https://gitlab.com/timete_OS/aes-light).

The main point of this effort is to prevent my friends to directly call the API with false data. And I don't have any friend smart enough to break the current encryption, or to find the key into the wasm binary.


## Experience

Honestly writing good interface is hard. I think Leptos found some good abstractions around it.

Overall I hated the web, it is complicated and over engineered. I thank mobile apps for slowly killing it. I don't want to become a web dev.

